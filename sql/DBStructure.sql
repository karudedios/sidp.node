DROP DATABASE SIDP;
CREATE DATABASE SIDP;
USE SIDP;

CREATE TABLE Suplidor (
    IdSuplidor INT AUTO_INCREMENT NOT NULL,
    RNCCedula VARCHAR(30) NOT NULL,
    Nombre VARCHAR(100) NOT NULL,
    Direccion VARCHAR(150) NOT NULL,
    Telefono VARCHAR(20) NOT NULL,
    Fax VARCHAR(20) NOT NULL,
    Correo VARCHAR(30) NOT NULL,
    Estatus BIT NOT NULL,
    CONSTRAINT PK_Suplidor PRIMARY KEY (IdSuplidor)
);

CREATE TABLE Contacto (
    IdContacto INT AUTO_INCREMENT NOT NULL,
    IdSuplidor INT NOT NULL,
    CONSTRAINT FK_ContactoSupidor FOREIGN KEY (IdSuplidor)
        REFERENCES Suplidor (IdSuplidor),
    Nombre VARCHAR(100) NOT NULL,
    Telefono VARCHAR(20) NOT NULL,
    Correo VARCHAR(30) NOT NULL,
    Puesto VARCHAR(30) NOT NULL,
    Estatus BIT NOT NULL,
    CONSTRAINT PK_Contacto PRIMARY KEY (IdContacto)
);

CREATE TABLE Componente (
    IdComponente INT AUTO_INCREMENT NOT NULL,
    Descripcion VARCHAR(100) NOT NULL,
    Estatus BIT NOT NULL,
    CONSTRAINT PK_Componente PRIMARY KEY (IdComponente)
);

CREATE TABLE Producto (
    IdProducto INT AUTO_INCREMENT NOT NULL,
    IdSuplidor INT NOT NULL,
    CONSTRAINT FK_ProductoSuplidor FOREIGN KEY (IdSuplidor)
        REFERENCES Suplidor (IdSuplidor),
    IdComponente INT NOT NULL,
    CONSTRAINT FK_ProductoComponente FOREIGN KEY (IdComponente)
        REFERENCES Componente (IdComponente),
    NombreComercial VARCHAR(30) NOT NULL,
    Densidad DECIMAL(10 , 2 ) NOT NULL,
    Estatus BIT NOT NULL,
    CONSTRAINT PK_Producto PRIMARY KEY (IdProducto)
);

CREATE TABLE TipoMuestra (
    IdTipoMuestra INT AUTO_INCREMENT NOT NULL,
    Descripcion VARCHAR(100) NOT NULL,
    Estatus BIT NOT NULL,
    CONSTRAINT PK_TipoMuestra PRIMARY KEY (IdTipoMuestra)
);

CREATE TABLE Muestra (
    IdMuestra INT AUTO_INCREMENT NOT NULL,
    IdTipoMuestra INT NOT NULL,
    CONSTRAINT FK_MuestraTipoMuestra FOREIGN KEY (IdTipoMuestra)
        REFERENCES TipoMuestra (IdTipoMuestra),
    IdProducto INT NOT NULL,
    CONSTRAINT FK_MuestraProducto FOREIGN KEY (IdProducto)
        REFERENCES Producto (IdProducto),
    CONSTRAINT PK_Muestra PRIMARY KEY (IdMuestra)
);

CREATE TABLE PreFormula (
    IdPreFormula INT AUTO_INCREMENT NOT NULL,
    Descripcion VARCHAR(100) NOT NULL,
    Estatus BIT NOT NULL,
    CONSTRAINT PK_PreFormula PRIMARY KEY (IdPreFormula)
);

CREATE TABLE DetallePreFormula (
    IdDetallePreFormula INT AUTO_INCREMENT NOT NULL,
    IdPreFormula INT NOT NULL,
    CONSTRAINT FK_DetallePreFormulaPreFormula FOREIGN KEY (IdPreFormula)
        REFERENCES PreFormula (IdPreFormula),
    IdComponente INT NOT NULL,
    CONSTRAINT FK_DetallePreFormulaComponente FOREIGN KEY (IdComponente)
        REFERENCES Componente (IdComponente),
    Porcentaje DECIMAL(10 , 2 ) NOT NULL,
    DensidadBase DECIMAL(10 , 2 ) NOT NULL,
    CONSTRAINT PK_DetallePreFormula PRIMARY KEY (IdDetallePreFormula)
);

CREATE TABLE Formula (
    IdFormula INT AUTO_INCREMENT NOT NULL,
    IdPreFormula INT NOT NULL,
    CONSTRAINT FK_FormulaPreFormula FOREIGN KEY (IdPreFormula)
        REFERENCES PreFormula (IdPreFormula),
    Kg DECIMAL(10 , 2 ) NULL,
    Gal DECIMAL(10 , 2 ) NULL,
    Densidad DECIMAL(10 , 2 ) NOT NULL,
    CONSTRAINT PK_Formula PRIMARY KEY (IdFormula)
);

CREATE TABLE DetalleFormula (
    IdDetalleFormula INT AUTO_INCREMENT NOT NULL,
    IdFormula INT NOT NULL,
    CONSTRAINT FK_DetalleFormulaFormula FOREIGN KEY (IdFormula)
        REFERENCES Formula (IdFormula),
    IdComponente INT NOT NULL,
    CONSTRAINT FK_DetalleFormulaComponente FOREIGN KEY (IdComponente)
        REFERENCES Componente (IdComponente),
    IdProducto INT NOT NULL,
    CONSTRAINT FK_DetalleFormulaProducto FOREIGN KEY (IdProducto)
        REFERENCES Producto (IdProducto),
    Porcentaje DECIMAL(10 , 2 ) NOT NULL,
    CalculoManual BIT NOT NULL,
    CONSTRAINT PK_DetalleFormula PRIMARY KEY (IdDetalleFormula)
);