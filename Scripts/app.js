var app = angular.module('SIDP', ['SIDP.controllers', 'SIDP.services', 'SIDP.factories', 'SIDP.directives', 'SIDP.filters', 'ngRoute']);

angular.module('SIDP.filters', []);
angular.module('SIDP.services', []);
angular.module('SIDP.factories', []);
angular.module('SIDP.directives', ['SIDP.services', 'SIDP.factories', 'SIDP.filters']);
angular.module('SIDP.controllers', ['SIDP.directives', 'SIDP.services', 'SIDP.factories', 'SIDP.filters']);

app.config(["$routeProvider", function ($routeProvider) {
	$routeProvider

	/* Default Routes */
		.when("/", { templateUrl: '/public/default.html' })

	/* Componente Routes */
		.when("/componente", { templateUrl: 'features/componente/index.html' })
		.when("/componente/:id", { templateUrl: 'features/componente/detail.html', controller: 'ComponenteController' })

	/* Contacto Routes */
		.when("/contacto", { templateUrl: 'features/contacto/index.html' })
		.when("/contacto/:id", { templateUrl: 'features/contacto/detail.html', controller: 'ContactoController' })

	/* Formula Routes */
		.when("/formula", { templateUrl: 'features/formula/index.html' })
		.when("/formula/:id", { templateUrl: 'features/formula/detail.html', controller: 'FormulaController' })

	/* Muestra Routes */
		.when("/muestra", { templateUrl: 'features/muestra/index.html' })
		.when("/muestra/:id", { templateUrl: 'features/muestra/detail.html', controller: 'MuestraController' })

	/* Producto Routes */
		.when("/producto", { templateUrl: 'features/producto/index.html' })
		.when("/producto/:id", { templateUrl: 'features/producto/detail.html', controller: 'ProductoController' })

	/* Suplidor Routes */
		.when("/suplidor", { templateUrl: 'features/suplidor/index.html' })
		.when("/suplidor/:id", { templateUrl: 'features/suplidor/detail.html', controller: 'SuplidorController' })

	/* Do I know you? No? Then come here. */
		.otherwise("/")
}]);