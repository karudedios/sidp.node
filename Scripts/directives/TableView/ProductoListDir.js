angular.module('SIDP.directives').directive("productoList", function() {
	return {
		restrict: 'E',
		scope: {
			displayHeaders: "=",
			displaySuplidor: "=",
			displayComponente: "=",
			fromSuplidor: "=",
			fromComponente: "="
		},
		templateUrl: '/public/templates/producto-list-template.html',
		controller: ["$scope", "DirControllerService",
		  function($scope ,  dirControllerService) {
	  		dirControllerService.HandleList($scope, "producto");
		  }]
	};
});