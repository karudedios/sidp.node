angular.module('SIDP.directives').directive("muestraList", function() {
	return {
		restrict: 'E',
		scope: {
			displayHeaders: "=",
			displayProducto: "=",
			fromProducto: "="
		},
		templateUrl: '/public/templates/muestra-list-template.html',
		controller: ["$scope", "DirControllerService",
		  function($scope ,  dirControllerService) {
	  		dirControllerService.HandleList($scope, "muestra");
		  }]
	};
});