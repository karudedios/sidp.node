angular.module('SIDP.directives').directive("contactoList", function() {
	return {
		restrict: 'E',
		scope: {
			displayHeaders: "=",
			displaySuplidor: "=",
			fromSuplidor: "="
		},
		templateUrl: '/public/templates/contacto-list-template.html',
		controller: ["$scope", "DirControllerService",
		  function($scope ,  dirControllerService) {
	  		dirControllerService.HandleList($scope, "contacto");
		  }]
	};
});