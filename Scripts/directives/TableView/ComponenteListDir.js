angular.module('SIDP.directives').directive("componenteList", function() {
	return {
		restrict: 'E',
		scope: {
			displayHeaders: "="
		},
		templateUrl: '/public/templates/componente-list-template.html',
		controller: ["$scope", "DirControllerService",
		  function($scope ,  dirControllerService) {
	  		dirControllerService.HandleList($scope, "componente");
		  }]
	};
});