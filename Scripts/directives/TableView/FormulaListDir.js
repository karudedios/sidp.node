angular.module('SIDP.directives').directive("formulaList", function() {
	return {
		restrict: 'E',
		scope: {
			displayHeaders: "="
		},
		templateUrl: '/public/templates/formula-list-template.html',
		controller: ["$scope", "DirControllerService",
		  function($scope ,  dirControllerService) {
	  		dirControllerService.HandleList($scope, "formula");
		  }]
	};
});