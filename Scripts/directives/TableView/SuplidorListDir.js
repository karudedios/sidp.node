angular.module('SIDP.directives').directive("suplidorList", function() {
	return {
		restrict: 'E',
		scope: {
			displayHeaders: "="
		},
		templateUrl: '/public/templates/suplidor-list-template.html',
		controller: ["$scope", "DirControllerService",
		  function($scope ,  dirControllerService) {
	  		dirControllerService.HandleList($scope, "suplidor");
		  }]
	};
});