var servicesModule = angular.module('SIDP.services');

servicesModule.factory("ValidatorService", function() {
	var returnObj = {};
	returnObj.patterns = {};
	returnObj.patterns.email = /^[a-z0-9!#$%&'*+\/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+\/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/;
	returnObj.patterns.phone = /^(\d{10}|\(?\d{1,3}\)?[ -]?\d{3}[ -]?\d{4})$/;
	returnObj.patterns.numbers = /^(\d+\.\d+)|(\d+)$/;


	return returnObj;
});