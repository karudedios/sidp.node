angular.module("SIDP.services").service("DirControllerService",
				  ["HttpService", "$location", "$routeParams", "ValidatorService",
  function(	httpService ,  $location ,  $routeParams , validatorService) {
		var formatCall = function(str) {
			var args = [].slice.call(arguments, 1);
			return str && str.replace(/{\d}/g, function(_) { return args.shift(); })
		}
  	return {
  		/**
  		 * This methods take care of the default handling for the lists, 
  		 * if needed, can be extended to support a higher customization
  		 * @param {Object} scope
  		 * @param {string} model
  		 * @return {Object} itself
  		*/
  		HandleList: function(scope, model) {
  			var getFormattedCall = function(model, id) {
  				return formatCall("/{0}/{1}", model, id || 0);
  			}

				scope.viewSuplidorDetails = function(id) {
					var path = getFormattedCall("suplidor", id);
					$location.path(path);
				}

				scope.viewContactoDetails = function(id) {
					var path = getFormattedCall("contacto", id);
					$location.path(path);
				}

				scope.viewComponenteDetails = function(id) {
					var path = getFormattedCall("componente", id);
					$location.path(path);
				}

				scope.viewProductoDetails = function(id) {
					var path = getFormattedCall("producto", id);
					$location.path(path);
				}

				scope.viewMuestraDetails = function(id) {
					var path = getFormattedCall("muestra", id);
					$location.path(path);
				}

				scope.viewTipoMuestraDetails = function(id) {
					var path = getFormattedCall("tipo-muestra", id);
					$location.path(path);
				}

				scope.viewFormulaDetails = function(id) {
					var path = getFormattedCall("formula", id);
					$location.path(path);
				}

				scope.viewPreFormulaDetails = function(id) {
					var path = getFormattedCall("formula-base", id);
					$location.path(path);
				}

				var getUrlStart = function() {
					if (scope.fromSuplidor) {
						return formatCall("/{0}/{1}", "suplidor", $routeParams.id);
					} else if (scope.fromProducto) {
						return formatCall("/{0}/{1}", "producto", $routeParams.id);						
					} else if (scope.fromComponente) {
						return formatCall("/{0}/{1}", "componente", $routeParams.id);						
					} else {
						return "";
					}
				}

				scope.query = function() {
					var path = formatCall("{0}/{1}/", getUrlStart(), model);
					httpService.call(path)
					.then(function(response) {
						scope.list = response.data;
					})
					.catch(function(response) {
						scope.error = response.error;
					});
				}		

				scope.delete = function(data) {
					var message = formatCall("Está seguro que desea eliminar este {0}?", model);
					if (confirm(message)) {
						scope.loading = true;
						httpService[model](data, "DELETE")
						.then(function(response) {
							scope.success = true;
							scope.message = response.data.message;
							data.status = false;
							//scope.list = scope.list.filter(function(x) { return x.id != data.id; });
						}).catch(function(response) {
							scope.success = false;
							scope.message = response.data.message;
						}).finally(function(response) {
							scope.loading = false;
						});
					}
				}

				scope.query();
				return this;
  		},
  		/**
  		 * This methods take care of the default handling for the details, 
  		 * if needed, can be extended to support a higher customization
  		 * @param {Object} scope
  		 * @param {string} model
  		 * @return {Object} itself
  		*/
  		HandleDetail: function(scope, model) {
  			scope.record = {}
				scope.patterns = validatorService.patterns;

				/* Transition Functions */
				scope.back = function () {
					history.back();
				}

  			/* Details Functions */
				scope.get = function() {
					var id = $routeParams.id;
					scope.isNew = id == 0;
					
					httpService[model](id, 'GET')
					.then(function(response) {
						if (response.data) {
							scope.record = angular.extend(scope.record, response.data);
							scope.isDisabled = scope.record == null && !scope.isNew;
						}
					})
					.catch(function(response) {
						scope.success = false;
						scope.message = response.data.message;
					});
				}

				scope.saveOrUpdate = function() {
					var promise;
					var data = angular.extend({}, scope.record);
					scope.loading = true;

					if (scope.isNew) {
						data.status = true;
						promise = httpService[model](data, "POST");
					} else {
						promise = httpService[model](data, "PUT");
					}

					promise.then(function(response) {
						scope.success = true;
						scope.message = response.data.message;
					}).catch(function(response) {
						scope.success = false;
						scope.message = response.data.message;
					}).finally(function(response) {
						scope.loading = false;
					});
				}

				scope.get();
				return this;
  		}
  	}
}]);