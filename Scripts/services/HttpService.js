var servicesModule = angular.module('SIDP.services');

servicesModule.factory("HttpService", ["$http", function($http) {
	var returnObj = {};

	var prepareCall = function prepareCall(url) {
		return function makeCall(data, method) {
			var reqObj = {}
			var isGet = typeof data == "string";

			reqObj.data = $.param(data);
			reqObj.method = method;
			reqObj.url = url

			if (!isGet) {
				reqObj.headers = { 'Content-Type': 'application/x-www-form-urlencoded' };
			} else {				
				reqObj.url += "/" + data;
			}

			return $http(reqObj);
		}
	}

	returnObj.componente = prepareCall("/componente");
	returnObj.contacto = prepareCall("/contacto");

	returnObj.formula = prepareCall("/formula");
	returnObj.preFormula = prepareCall("/preformula");

	returnObj.muestra = prepareCall("/muestra");
	returnObj.tipoMuestra = prepareCall("/tipomuestra");

	returnObj.producto = prepareCall("/producto");
	returnObj.suplidor = prepareCall("/suplidor");
	
	returnObj.call = function(url) {
		return prepareCall(url)({}, "GET");
	};

	return returnObj;
}]);