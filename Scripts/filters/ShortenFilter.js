var filterModule = angular.module("SIDP.filters");

filterModule.filter("shorten", function () {
	return function(input, length) {
		if (!input) {
			return "";
		}

		length = length || input.length;
		return (input.length > length) ? input.slice(0, length).trim() + "..." : input;
	}
});