var express = require('express');
var bodyParser = require('body-parser')
var path = require('path');

var routes = require('./routes/routes.js')


var pub = __dirname;
var app = express();
var router = express.Router();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(pub));
app.use(express.static(path.join(pub, 'public')));

// Componente
router
	.get("/componente", routes.componente.query)
	.get("/componente/:id", routes.componente.get)
	.get("/componente/:idComponente/producto", routes.componente.queryProducto)
	.post("/componente", routes.componente.post)
	.put("/componente", routes.componente.put)
	.delete("/componente", routes.componente.deactivate);

// Contacto
router
	.get("/contacto", routes.contacto.query)
	.get("/contacto/:id", routes.contacto.get)
	.post("/contacto", routes.contacto.post)
	.put("/contacto", routes.contacto.put)
	.delete("/contacto", routes.contacto.deactivate);

// Formula
router
	.get("/formula", routes.formula.query)
	.get("/formula/:id", routes.formula.get)
	.post("/formula", routes.formula.post)
	.put("/formula", routes.formula.put);

// Pre-Formula
router
	.get("/preformula", routes.preFormula.query)
	.get("/preformula/:id", routes.preFormula.get)
	.post("/preformula", routes.preFormula.post)
	.put("/preformula", routes.preFormula.put);

// Muestra
router
	.get("/muestra", routes.muestra.query)
	.get("/muestra/:id", routes.muestra.get)
	.post("/muestra", routes.muestra.post)
	.put("/muestra", routes.muestra.put);

// Tipo Muestra
router
	.get("/tipomuestra", routes.tipoMuestra.query)
	.get("/tipomuestra/:id", routes.tipoMuestra.get)
	.post("/tipomuestra", routes.tipoMuestra.post)
	.put("/tipomuestra", routes.tipoMuestra.put);

// Producto
router
	.get("/producto", routes.producto.query)
	.get("/producto/:id", routes.producto.get)
	.get("/producto/:idProducto/muestra", routes.producto.queryMuestra)
	.post("/producto", routes.producto.post)
	.put("/producto", routes.producto.put)
	.delete("/producto", routes.producto.deactivate);

// Suplidor
router
	.get("/suplidor", routes.suplidor.query)
	.get("/suplidor/:id", routes.suplidor.get)
	.get("/suplidor/:idSuplidor/producto", routes.suplidor.queryProducto)
	.get("/suplidor/:idSuplidor/contacto", routes.suplidor.queryContacto)
	.post("/suplidor", routes.suplidor.post)
	.put("/suplidor", routes.suplidor.put)
	.delete("/suplidor", routes.suplidor.deactivate);

// Test
router
	.get("/test/:idSuplidor", routes.test)

app.use("/", router);
app.get('*', function (req, res) {
  res.sendFile('/public/index.html');
});

app.listen(8080);
console.log("App started on port :8080")