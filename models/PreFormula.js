module.exports = function(sequelize, DataTypes) {
	var PreFormula = sequelize.define("PreFormula", {
		id: {
			type: DataTypes.INTEGER,
			field: 'IdPreFormula'
		},
		descripcion: {
			type: DataTypes.STRING
		},
		status: {
			type: DataTypes.BOOLEAN,
			field: 'Estatus'
		}
	}, {
  	freezeTableName: true,
  	timestamps: false,
  	classMethods: {
  		associate: function(models) {
  			PreFormula.hasMany(models.Formula, { foreignKey: 'idPreFormula' });
  		}
  	}
	});

	return PreFormula;
}