module.exports = function(sequelize, DataTypes) {
	var Contacto = sequelize.define('Contacto', {		
		id: {
		  type: DataTypes.INTEGER,
		  field: 'IdContacto'
		},
		nombre: {
			type: DataTypes.STRING
		},
		telefono: {
			type: DataTypes.STRING
		},
		correo: {
			type: DataTypes.STRING
		},
		puesto: {
			type: DataTypes.STRING
		},
		status: {
			type: DataTypes.BOOLEAN,
			field: 'Estatus'
		},
	}, {
  	freezeTableName: true,
  	timestamps: false,
  	classMethods: {
  		associate: function(models) {
  			Contacto.belongsTo(models.Suplidor, { foreignKey: 'IdSuplidor' });
  		}
  	}
	});

	return Contacto;
}