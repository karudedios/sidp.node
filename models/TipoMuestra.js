module.exports = function(sequelize, DataTypes) {
	var TipoMuestra = sequelize.define("TipoMuestra", {
		id: {
			type: DataTypes.INTEGER,
			field: 'IdTipoMuestra'
		},
		descripcion: {
			type: DataTypes.STRING
		},
		status: {
			type: DataTypes.BOOLEAN,
			field: 'Estatus'
		}
	}, {
  	freezeTableName: true,
  	timestamps: false,
  	classMethods: {
  		associate: function(models) {
  			TipoMuestra.hasMany(models.Muestra, { foreignKey: 'idTipoMuestra' })
  		}
  	}
	});

	return TipoMuestra;
}