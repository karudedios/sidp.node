module.exports = function(sequelize, DataTypes) {
	var Formula = sequelize.define("Formula", {
		id: {
			type: DataTypes.INTEGER,
			field: 'IdFormula'
		},	
		idPreFormula: {
			type: DataTypes.INTEGER
		},
		kg: {
			type: DataTypes.FLOAT
		},
		gal: {
			type: DataTypes.FLOAT
		},
		densidad: {
			type: DataTypes.FLOAT
		}
	}, {	
  	freezeTableName: true,
  	timestamps: false,
  	classMethods: {
  		associate: function(models) {
  			Formula.belongsTo(models.PreFormula, { foreignKey: 'idPreFormula' });
  		}
  	}
	});

	return Formula;
}