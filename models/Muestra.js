module.exports = function(sequelize, DataTypes) {
	var Muestra = sequelize.define("Muestra", {
		id: {
			type: DataTypes.INTEGER,
			field: 'IdMuestra'
		},
		idProducto: {
			type: DataTypes.INTEGER
		}
	}, {
  	freezeTableName: true,
  	timestamps: false,
  	classMethods: {
  		associate: function(models) {
  			Muestra.belongsTo(models.TipoMuestra, { foreignKey: 'idTipoMuestra' }),
  			Muestra.belongsTo(models.Producto, { foreignKey: 'idProducto' })
  		}
  	}
	});

	return Muestra;
}