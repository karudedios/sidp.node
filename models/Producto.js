module.exports = function(sequelize, DataTypes) {
	var Producto = sequelize.define("Producto", {
		id: {
			type: DataTypes.INTEGER,
			field: "idProducto"
		},
		nombreComercial: {
			type: DataTypes.STRING
		},
		densidad: {
			type: DataTypes.FLOAT
		},
		status: {
			type: DataTypes.BOOLEAN,
			field: "Estatus"
		}
	}, {
  	freezeTableName: true,
  	timestamps: false,
  	classMethods: {
  		associate: function(models) {
  			Producto.belongsTo(models.Suplidor, { foreignKey: "idSuplidor" })
  			Producto.belongsTo(models.Componente, { foreignKey: "idComponente" })
  			Producto.hasMany(models.Muestra, { foreignKey: "idProducto" })
  		}
  	}
	});

	return Producto;
}