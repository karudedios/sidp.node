module.exports = function(sequelize, DataTypes) {
	var Suplidor = sequelize.define('Suplidor', {		
		id: {
		  type: DataTypes.INTEGER,
		  field: 'IdSuplidor'
		},
		rncCedula: {
			type: DataTypes.STRING
		},
		nombre: {
			type: DataTypes.STRING
		},
		direccion: {
			type: DataTypes.STRING
		},
		telefono: {
			type: DataTypes.STRING
		},
		fax: {
			type: DataTypes.STRING
		},
		correo: {
			type: DataTypes.STRING
		},
		status: {
			type: DataTypes.BOOLEAN,
			field: 'Estatus'
		},
	}, {
  	freezeTableName: true,
  	timestamps: false,
  	classMethods: {
  		associate: function(models) {
  			Suplidor.hasMany(models.Contacto, { foreignKey: 'IdSuplidor' });
  			Suplidor.hasMany(models.Producto, { foreignKey: 'IdSuplidor' });
  		}
  	}
	});

	return Suplidor;
}