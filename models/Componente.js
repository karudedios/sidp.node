module.exports = function(sequelize, DataTypes) {
	var Componente = sequelize.define("Componente", {
		id: {
			type: DataTypes.INTEGER,
			field: 'IdComponente'
		},
		descripcion: {
			type: DataTypes.STRING
		},
		status: {
			type: DataTypes.BOOLEAN,
			field: 'Estatus'
		}
	}, {
  	freezeTableName: true,
  	timestamps: false,
  	classMethods: {
  		associate: function(models) {
  			Componente.hasMany(models.Producto, { foreignKey: 'idComponente' })
  		}
  	}
	});

	return Componente;
}