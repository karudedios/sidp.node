var controllerService = angular.module("SIDP.controllers");

controllerService.controller("ContactoController", [
					"$scope", "DirControllerService", "HttpService",
 	function($scope ,  dirControllerService ,  httpService) {

		dirControllerService.HandleDetail($scope, "contacto");

		httpService.suplidor({}, "GET")
		.then(function(suplidores) {
			$scope.suplidores = suplidores.data;
		})
		.catch(function(error) {
			$scope.success = false;
			$scope.message = response.data.message;
		});

		return;
	}
]);