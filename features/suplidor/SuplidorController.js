var controllerModule = angular.module("SIDP.controllers");

controllerModule.controller("SuplidorController", [
	  				"$scope", "DirControllerService"
	, function($scope ,  dirControllerService) {
		
		dirControllerService.HandleDetail($scope, "suplidor");

		$scope.displayDetail = true;

		$scope.viewDetailTab = function() {
			$scope.displayContactos = $scope.displayProductos = !($scope.displayDetail = true);
		}

		$scope.viewContactosTab = function() {
			$scope.renderContactos = true;
			$scope.displayDetail = $scope.displayProductos = !($scope.displayContactos = true);			
		}

		$scope.viewProductosTab = function() {
			$scope.renderProductos = true;
			$scope.displayDetail = $scope.displayContactos = !($scope.displayProductos = true);			
		}

		return;
	}
]);	