var controllerService = angular.module("SIDP.controllers");

controllerService.controller("ComponenteController", [
					"$scope", "DirControllerService",
 	function($scope ,  dirControllerService) {

		dirControllerService.HandleDetail($scope, "componente");

		$scope.displayDetail = true;
		
		$scope.viewDetailTab = function() {
			$scope.displayProductos = !($scope.displayDetail = true);
		}

		$scope.viewProductosTab = function() {
			$scope.renderProductos = true;
			$scope.displayDetail = !($scope.displayProductos = true);
		}

		return;
	}
]);