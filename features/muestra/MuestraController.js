var controllerService = angular.module("SIDP.controllers");

controllerService.controller("MuestraController", [
					"$scope", "DirControllerService", "HttpService",
 	function($scope ,  dirControllerService ,  httpService) {

		dirControllerService.HandleDetail($scope, "muestra");

		httpService.tipoMuestra({}, "GET")
		.then(function(tipoMuestras) {
			$scope.tipoMuestras = tipoMuestras.data;
		})
		.catch(function(error) {
			$scope.success = false;
			$scope.message = response.data.message;
		});

		httpService.producto({}, "GET")
		.then(function(productos) {
			$scope.productos = productos.data;
		})
		.catch(function(error) {
			$scope.success = false;
			$scope.message = response.data.message;
		});

		return;
	}
]);