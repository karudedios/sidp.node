var controllerService = angular.module("SIDP.controllers");

controllerService.controller("FormulaController", [
					"$scope", "DirControllerService", "HttpService",
 	function($scope ,  dirControllerService ,  httpService) {

		dirControllerService.HandleDetail($scope, "formula");

		httpService.preFormula({}, "GET")
		.then(function(preFormulas) {
			$scope.preFormulas = preFormulas.data;
		})
		.catch(function(error) {
			$scope.success = false;
			$scope.message = response.data.message;
		});

		return;
	}
]);