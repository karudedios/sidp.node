var controllerService = angular.module("SIDP.controllers");

controllerService.controller("ProductoController", [
					"$scope", "DirControllerService", "HttpService",
 	function($scope ,  dirControllerService ,  httpService) {
		
		dirControllerService.HandleDetail($scope, "producto");

		$scope.displayDetail = true;
		
		$scope.viewDetailTab = function() {
			$scope.displayMuestras = !($scope.displayDetail = true);
		}

		$scope.viewMuestrasTab = function() {
			$scope.renderMuestras = true;
			$scope.displayDetail = !($scope.displayMuestras = true);
		}

		httpService.suplidor({}, "GET")
		.then(function(suplidores) {
			$scope.suplidores = suplidores.data;
		})
		.catch(function(error) {
			$scope.success = false;
			$scope.message = response.data.message;
		});

		httpService.componente({}, "GET")
		.then(function(componentes) {
			$scope.componentes = componentes.data;
		})
		.catch(function(error) {
			$scope.success = false;
			$scope.message = response.data.message;
		});

		return;
	}
]);