var models = require("../models")

/**
 * GenerateFKCall
 * This method is a Higher-Order function
 * and it's purpose is to generate either a find or findAll Call
 * @params {string} method (either find, or findAll)
 * @params {object} whereCondition, the condition to be used in the where clause
 * @returns GetAssociatesToInclude
*/
var GenerateFKCall = function(method) {
	/**
	 * GetAssociatesToInclude
	 * This method is a Higher-Order function
	 * and it's purpose is to generate the call specified on GenerateFKCall
	 * for the specified Model, with specified Associates for specific Attributes
	 * @params Model {string} the Model
	 * @params FKs {array{string}} the Associated Models
	 * @params Attributes {array{string}} the attributes - if null will get all attributes
	 * @returns The req/res function for express' routing
	*/
	return function GetAssociatesToInclude(Model, FKs, Attributes, whereCondition) {
		Attributes = Attributes || [];

		var foreignKeys = FKs.map(function(fk) {
			var objReturn = {};
			var attribute = Attributes.shift() || []

			objReturn.model = models[fk]

			if (attribute.length) {
				objReturn.attributes = attribute;
			}

			return objReturn;
		});

		return function(req, res) {
			var applyWhere = whereCondition && whereCondition.reduce(function(object, condition) {
				object[condition] = (req.params[condition] || req.body[condition])
				return object;
			}, {});
			
			var where = applyWhere || (method == "find" ? { id : req.params.id } : {})

			models[Model][method]({
				where: where,
				include : foreignKeys
			})
			.then(function(response) {
				res.json(response);
			})
			.catch(function(response) {
				res.status(500).json(response)
			});
		}
	}
}

var queryWithFK = GenerateFKCall("findAll");
var getWithFK = GenerateFKCall("find");

var GenerateQueryFor = function(Model) {
	return function(req, res) {
		console.log(req.params, req.body)
		models[Model].findAll().then(function (response) {
			res.json(response);
		})
		.catch(function(response) {
			res.status(500).json(response)
		});
	}
}

var GenerateGetFor = function(Model) {
	return function(req, res) {
		models[Model].find({ where: { id: req.params.id }})
		.then(function(response) {
			res.json(response);
		})
		.catch(function(response) {
			res.status(500).json(response)
		});
	}
}

var GeneratePostFor = function (Model) {
	return function(req, res) {
		models[Model].create(req.body)
		.then(function(response) {
			res.json({ success: true, message: Model + " Agregado satisfactoriamente" });
		})
		.catch(function(response) {
			res.status(409).json({ success: false, message:"Ha ocurrido un error al intentar agregar el " + Model + ", intente más tarde." });
		});
	}
}


var GeneratePutFor = function (Model) {
	return function(req, res) {
		models[Model].update(req.body, { where: { id: req.body.id }})
		.then(function(response) {
			res.json({ success: true, message: Model + " Actualizado satisfactoriamente" });
		})
		.catch(function(response) {
			res.status(409).json({ success: false, message:"Ha ocurrido un error al intentar actualizar el " + Model + ", intente más tarde." });
		});
	}	
}

var GenerateGetPostPutQuery = function(Model) {
  this.query = GenerateQueryFor(Model);
	this.get = GenerateGetFor(Model);
	this.post = GeneratePostFor(Model);
	this.put = GeneratePutFor(Model);
}

var DefaultDelete = function (Model) {
	return function(req, res) {
		models[Model].update({ status: false } ,{ where: { id: req.body.id }})
		.then(function(response) {
			res.json({ success: true, message: Model + " desactivado satisfactoriamente" });
		})
		.catch(function(response) {
			res.status(409).json({ success: false, message:"Ha ocurrido un error al intentar eliminar el suplidor, intente más tarde." });
		});
	}
};

var prototypeInherit = function(from, initParam) {
	var newObj = {};
	newObj.__proto__ = from;
	newObj.constructor = from.constructor;
	from.call(newObj, initParam);
	return newObj;
}

var defaultCRUDFor = function(Model) {
	return prototypeInherit(GenerateGetPostPutQuery, Model);
}

var ComponenteCalls	= new defaultCRUDFor("Componente");
var ContactoCalls		= new defaultCRUDFor("Contacto");
var FormulaCalls		= new defaultCRUDFor("Formula");
var PreFormulaCalls	= new defaultCRUDFor("PreFormula");
var MuestraCalls		= new defaultCRUDFor("Muestra");
var TipoMuestraCalls		= new defaultCRUDFor("TipoMuestra");
var ProductoCalls		= new defaultCRUDFor("Producto");
var SuplidorCalls		= new defaultCRUDFor("Suplidor");

ComponenteCalls.deactivate = DefaultDelete("Componente");
ContactoCalls.deactivate = DefaultDelete("Contacto");
ProductoCalls.deactivate = DefaultDelete("Producto");
SuplidorCalls.deactivate = DefaultDelete("Suplidor");

/* Componente Specific */
ComponenteCalls.queryProducto = queryWithFK("Producto", ["Suplidor"], [["id", "nombre"]], [ "idComponente" ]);


/* Suplidor Specific */
SuplidorCalls.get = getWithFK("Suplidor", ["Contacto", "Producto"]);
SuplidorCalls.queryContacto = queryWithFK("Contacto", [], [], [ "idSuplidor" ]);
SuplidorCalls.queryProducto = queryWithFK("Producto", ["Componente"], [["id", "descripcion"]], [ "idSuplidor" ]);


/* Contacto Specific */
ContactoCalls.query = queryWithFK("Contacto", ["Suplidor"], [["id", "nombre"]]);
ContactoCalls.get = getWithFK("Contacto", ["Suplidor"], [["id", "nombre"]]);


/* Producto Specific */
ProductoCalls.query = queryWithFK("Producto", ["Suplidor", "Componente"], [["id", "nombre"]	, ["id", "descripcion"]]);
ProductoCalls.get = getWithFK("Producto", ["Suplidor", "Componente", "Muestra"], [["id", "nombre"]	, ["id", "descripcion"]]);
ProductoCalls.queryMuestra = queryWithFK("Muestra", ["TipoMuestra"], [["id", "descripcion"]], [ "idProducto" ]);


/* Muestra Specific*/
MuestraCalls.query = queryWithFK("Muestra", ["TipoMuestra", "Producto"], [["id", "descripcion"]	, ["id", "nombreComercial"]]);
MuestraCalls.get = getWithFK("Muestra", ["TipoMuestra", "Producto"], [["id", "descripcion"]	, ["id", "nombreComercial"]]);


/* Formula Specific */
FormulaCalls.query = queryWithFK("Formula", ["PreFormula"], [["id", "descripcion"]]);
FormulaCalls.get = getWithFK("Formula", ["PreFormula"], [["id", "descripcion"]]);


module.exports = {
	componente: ComponenteCalls,
	contacto: ContactoCalls,
	formula: FormulaCalls,
	preFormula: PreFormulaCalls,
	muestra: MuestraCalls,
	tipoMuestra: TipoMuestraCalls,
	producto: ProductoCalls,
	suplidor: SuplidorCalls,
	test: queryWithFK("Contacto", [], [], [])
};
